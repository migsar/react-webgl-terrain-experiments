# react-webgl-terrain-experiments

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

You need to set two env variables, check example file.

It is based on [Bringing 3D terrain to the browser with Three.js](https://blog.mapbox.com/bringing-3d-terrain-to-the-browser-with-three-js-410068138357)
