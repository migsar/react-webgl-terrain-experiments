import React, { Component, createRef } from 'react';
import * as mapbox from 'mapbox-gl/dist/mapbox-gl';
import * as SphericalMercator from '@mapbox/sphericalmercator';

import { print } from '../../utils/commons';
import { px2tile, offset, transformToElevation } from '../../utils/maps';

import Terrain from '../terrain/Terrain';

import './App.css';
import 'mapbox-gl/dist/mapbox-gl.css';

mapbox.accessToken = process.env['REACT_APP_MAPBOX_TOKEN'];
const TILE_SIZE = process.env['REACT_APP_TILE_SIZE'];

// Create a new projection
const sm = new SphericalMercator({ size: TILE_SIZE });

class App extends Component {
  constructor(props) {
    super(props);
    this.canvas = createRef();
    this.state = {
      surface: null
    };
  }

  componentDidMount() {
    this.map = new mapbox.Map({
      container: 'map',
      center: [-109.84842122026924, 23.737353088852032],
      zoom: 10,
      style: 'mapbox://styles/mapbox/streets-v11'
    });

    this.context = this.canvas.current.getContext('2d');
  }

  addTile(tile) {
    const { context } = this;

    return new Promise((resolve, reject) => {
      const imgObj = new Image();

      imgObj.onload = () => {
        context.drawImage(imgObj, tile.px, tile.py);
        resolve();
      };

      imgObj.src = tile.img;
    });
  }

  processMap = () => {
    const { map, context } = this;
    const { _ne, _sw } = map.getBounds();
    const zoom = Math.floor(map.getZoom());

    // 0: Top-Left, 1: Top-Right, 2: Bottom-Right, 3: Bottom-Left & 4: Top-Left repeated to close the cycle
    const bbox = [
      [ _sw.lng, _ne.lat ],
      [ _ne.lng, _ne.lat ],
      [ _ne.lng, _sw.lat ],
      [ _sw.lng, _sw.lat ],
      [ _sw.lng, _ne.lat ]
    ];

    // todo: Optimize zoom for getting alitude data
    // Get viewport coordinates here
    //    const polygon = {
    //      type: 'Polygon',
    //      coordinates: [bbox]
    //    };
    //    // Calculate the minimum tiles needed here
    //    const myTiles = tiles(polygon, { min_zoom: zoom, max_zoom: zoom});
    //    print(myTiles);
    //    print(`Zoom tile length: ${Math.pow(2,zoom)}`);

    // Get top-left(NW) and bottom-right(SE) corner pixel positions
    const nwPx = sm.px(bbox[0], zoom);
    const sePx = sm.px(bbox[2], zoom);

    // Each offset is relative to its tile, that is, those are not necessarily related
    const nw = {
      coords: bbox[0],
      pixels: nwPx,
      tiles: nwPx.map(px2tile),
      pxOffset: nwPx.map(offset)
    };

    const se = {
      coords: bbox[2],
      pixels: sePx,
      tiles: sePx.map(px2tile),
      pxOffset: sePx.map(offset)
    };

    const expectedHeight = se.pixels[1] - nw.pixels[1];
    const expectedWidth = se.pixels[0] - nw.pixels[0];

    const tiles = [];
    for(let i = nw.tiles[0]; i <= se.tiles[0]; i++) {
      for(let j = nw.tiles[1]; j <= se.tiles[1]; j++) {
        print(`Get tile x: ${i}, y: ${j}`);
        tiles.push({
          x: i,
          y: j,
          z: zoom,
          px: (i - nw.tiles[0]) * TILE_SIZE - nw.pxOffset[0],
          py: (j - nw.tiles[1]) * TILE_SIZE - nw.pxOffset[1]
        });
      }
    }

    const loadTile = tile => fetch(`https://api.mapbox.com/v4/mapbox.terrain-rgb/${tile.z}/${tile.x}/${tile.y}@2x.pngraw?access_token=${mapbox.accessToken}`);
    Promise.all(tiles.map(loadTile))
      .then(data => {
        return Promise.all(data.map(response => response.blob()));
      })
      .then(blobs => {
        return Promise.all(blobs.map((blob, i) => this.addTile({...tiles[i], img: URL.createObjectURL(blob)})));
      })
      .then(() => {
        const imageOptions = {
          height: expectedHeight,
          width: expectedWidth
        };

        const elevationImage = context.getImageData(0, 0, expectedWidth, expectedHeight);
        const elevation = transformToElevation(elevationImage, imageOptions);
        this.setState({
          surface: elevation
        });
      })
      .catch(err => {
        console.error(err);
      });
  }

  render() {
    const { surface } = this.state;

    return (
      <div className="app">
        <main>
          <div className="map" id="map" />
          <canvas ref={this.canvas} width="400" height="300" />
          { surface && <Terrain surface={ surface } />}
          <button type="button" onClick={this.processMap}>Click!</button>
        </main>
      </div>
    );
  }
}

export default App;
