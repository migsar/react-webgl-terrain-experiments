const TILE_SIZE = process.env['REACT_APP_TILE_SIZE'];

// Get the tile coordinate of a pixel
export const px2tile = pixelCoord => Math.floor(pixelCoord / TILE_SIZE);
// Get the offset of a pixel relative to tile
export const offset = pixelCoord => Math.floor(pixelCoord % TILE_SIZE);

export function transformToElevation(image){
  const cols = image.width;
  const rows = image.height;
  const channels = 4;
  const output = [];

  let min = Infinity;
  let max = -Infinity

  const upsampledResolution = {};

  for (let r = 0; r < rows; r++){

    let lastElev = null;
    let consecutiveCount = 0;

    for (let c = 0; c < cols; c++){
      const currentPixelIndex = (r*cols+c) * channels;
      const R = image.data[currentPixelIndex];
      const G = image.data[currentPixelIndex+1];
      const B = image.data[currentPixelIndex+2];

      const elev = (R * 256 * 256 + G * 256 + B)/10-10000;
      if (elev<min) min = elev
      if (elev>max) max = elev

      if (elev === lastElev) consecutiveCount++

      else {
        if (upsampledResolution[consecutiveCount]) upsampledResolution[consecutiveCount]++
        else upsampledResolution[consecutiveCount] = 1
        consecutiveCount = 0
      }

      lastElev = elev
      output.push(elev)
    }
  }

  return {
    comments: 'Elevation data is presented as a one-dimensional, row-major array. Consecutive points are in the same row west to east, with rows arranged north to south.',
    elevations: output,
    columns: cols,
    rows: rows,
    min: min,
    max: max,
    zoom: 10
  }
}

