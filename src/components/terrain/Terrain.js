import React, { Component, createRef } from 'react';
import {
  Scene,
  Group,
  AmbientLight,
  PerspectiveCamera,
  WebGLRenderer,
  PlaneGeometry,
  MeshBasicMaterial,
  MeshPhongMaterial,
  Mesh
} from 'three';

class Terrain extends Component {
  constructor(props) {
    super(props);

    this.threeCanvas = createRef();
  }

  componentDidMount() {
    const { surface } = this.props;
    const scene = new Scene();
    const camera = new PerspectiveCamera( 100, 4 / 3, 0.1, 1000 );
    const light = new AmbientLight( 0xffffff ); // soft white light

    light.position.x = surface.columns;
    light.position.z = surface.rows;
    light.position.y = 150;

    const rendererParams = {
      canvas: this.threeCanvas.current
    };

    const renderer = this.renderer = new WebGLRenderer(rendererParams);

    const planeParams = surface ?
      [
        surface.columns,
        surface.rows,
        surface.columns - 1,
        surface.rows - 1
      ] :
      [0, 0, 0, 0];
    const geometry = new PlaneGeometry(...planeParams);
    const material = new MeshPhongMaterial( { color: 0xff0000, wireframe: false } );

    const m = elev => {
      const base = 0;
      const amplitude = 250;

      const delta = surface.max - surface.min;
      const eHeight = (elev - surface.min) / delta;

      return base + (eHeight * amplitude);
    };

    for (var i = 0; i < geometry.vertices.length; i++) geometry.vertices[i].z = m(surface.elevations[i]);
    geometry.computeVertexNormals();

    const terrain = new Group();
    const terrainMesh = new Mesh( geometry, material );

    terrain.rotateX(Math.PI / 2);
    terrain.add( terrainMesh );

    scene.add(light);
    scene.add(terrain);
    camera.position.x = 100;
    camera.position.y = 100;
    camera.position.z = 300;

    var animate = function () {
      requestAnimationFrame( animate );

      // cube.rotation.x += 0.01;
      // terrain.rotation.y += 0.01;

      renderer.render( scene, camera );
    };

    animate();
  }

  render() {
    return <canvas ref={this.threeCanvas} width="400" height="300" />;
  }
}

export default Terrain;
